<?php
namespace Innomedio\UserBundle\Controller\Backend;

use Innomedio\BackendThemeBundle\Controller\BackendThemeController;
use Innomedio\BackendThemeBundle\Service\Ajax\AjaxResponse;
use Innomedio\UserBundle\Entity\BackendUserRight;
use Innomedio\UserBundle\Form\BackendUserRightType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Security("has_role('ROLE_ROOT')")
 * @Route("/access/rights")
 */
class BackendUserRightController extends BackendThemeController
{
    /**
     * @Route("/", name="innomedio.user.rights.list")
     * @return Response
     */
    public function list()
    {
        $this->header()->addBreadcrumb('innomedio.user.access', $this->generateUrl('innomedio.user.access'));
        $this->header()->addBreadcrumb('innomedio.user.user_rights');

        return $this->render('@InnomedioUser/backend/rights/list.html.twig', array(
            'rights' => $this->getDoctrine()->getRepository('InnomedioUserBundle:BackendUserRight')->findAll()
        ));
    }

    /**
     * @Route(
     *     "/{action}/{id}",
     *     name="innomedio.user.rights.form",
     *     requirements={
     *         "action" = "add|edit",
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     * )
     *
     * @param $action
     * @param BackendUserRight|null $right
     * @return Response
     */
    public function form($action, BackendUserRight $right = null)
    {
        $this->header()->addBreadcrumb('innomedio.user.access', $this->generateUrl('innomedio.user.access'));
        $this->header()->addBreadcrumb('innomedio.user.user_rights', $this->generateUrl('innomedio.user.rights.list'));
        $this->header()->addBreadcrumb('innomedio.user.rights.title.' . $action);

        $form = $this->createForm(BackendUserRightType::class, $right);
        $form->add('submit', SubmitType::class, array(
            'label' => 'innomedio.backend_theme.global.' . $action
        ));

        return $this->render('@InnomedioUser/backend/rights/form.html.twig', array(
            'form' => $form->createView(),
            'activeNav' => 'access',
            'activeSubnav' => 'backend_rights'
        ));
    }

    /**
     * @Route(
     *     "/submit/{id}",
     *     name="innomedio.user.rights.submit"),
     *     requirements={
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     *
     * @param $id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function submit($id, Request $request)
    {
        $response = new AjaxResponse();

        if ($id > 0) {
            $right = $this->getDoctrine()->getRepository('InnomedioUserBundle:BackendUserRight')->find($id);
            $action = 'edit';
        } else {
            $right = new BackendUserRight();
            $action = 'add';
        }

        $form = $this->createForm(BackendUserRightType::class, $right);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($right);
            $em->flush();

            $response->setSuccess(true);
            $response->setRedirect($this->generateUrl('innomedio.user.rights.list'));
            $response->setMessage(true);
        } else {
            $response->setErrors($this->get('innomedio.backend_theme.error_parser')->getErrors($form, $right));
        }

        return new JsonResponse($response->getResponse());
    }

    /**
     * @Route("/remove/{id}", requirements={"id" = "\d+"}, name="innomedio.user.rights.remove")

     * @param BackendUserRight $right
     *
     * @return JsonResponse
     */
    public function ajaxRemove(BackendUserRight $right)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($right);
        $em->flush();

        return new JsonResponse(array(
            'success' => true
        ));
    }
}