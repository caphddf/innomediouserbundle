<?php
namespace Innomedio\UserBundle\Controller\Backend;

use Innomedio\BackendThemeBundle\Controller\BackendThemeController;
use Innomedio\BackendThemeBundle\Service\Ajax\AjaxResponse;
use Innomedio\UserBundle\Entity\BackendUser;
use Innomedio\UserBundle\Form\BackendUserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Security("has_role('ROLE_BACKEND_USERS')")
 * @Route("/access/users")
 */
class BackendUserController extends BackendThemeController
{
    /**
     * @Route("/", name="innomedio.user.users.list")
     * @return Response
     */
    public function list()
    {
        $this->header()->addBreadcrumb('innomedio.user.access', $this->generateUrl('innomedio.user.access'));
        $this->header()->addBreadcrumb('innomedio.user.users');

        return $this->render('@InnomedioUser/backend/users/list.html.twig', array(
            'users' => $this->getDoctrine()->getRepository('InnomedioUserBundle:BackendUser')->findAll()
        ));
    }

    /**
     * @Route(
     *     "/{action}/{id}",
     *     name="innomedio.user.users.form",
     *     requirements={
     *         "action" = "add|edit",
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     * )
     *
     * @param $action
     * @param BackendUser|null $user
     * @return Response
     */
    public function form($action, BackendUser $user = null)
    {
        if (!$user) {
            $user = new BackendUser();
        }

        $this->header()->addBreadcrumb('innomedio.user.access', $this->generateUrl('innomedio.user.access'));
        $this->header()->addBreadcrumb('innomedio.user.users', $this->generateUrl('innomedio.user.users.list'));
        $this->header()->addBreadcrumb('innomedio.user.users.title.' . $action);

        $form = $this->createForm(BackendUserType::class, $user);
        $form->add('submit', SubmitType::class, array(
            'label' => 'innomedio.backend_theme.global.' . $action
        ));

        return $this->render('@InnomedioUser/backend/users/form.html.twig', array(
            'form' => $form->createView(),
            'activeNav' => 'access',
            'activeSubnav' => 'backend_users'
        ));
    }

    /**
     * @Route(
     *     "/submit/{id}",
     *     name="innomedio.user.users.submit"),
     *     requirements={
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     *
     * @param $id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function submit($id, Request $request)
    {
        $response = new AjaxResponse();

        if ($id > 0) {
            $user = $this->getDoctrine()->getRepository('InnomedioUserBundle:BackendUser')->find($id);
            $action = 'edit';
        } else {
            $user = new BackendUser();
            $action = 'add';
        }

        $activePassword = $user->getPassword();

        $form = $this->createForm(BackendUserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setUsername($user->getEmail());

            if (!empty($form->get('password')->getData())) {
                $encodedPassword = $this->get('security.password_encoder')->encodePassword($user, $user->getPassword());
                $user->setPassword($encodedPassword);
            } else {
                $user->setPassword($activePassword);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $response->setSuccess(true);
            $response->setRedirect($this->generateUrl('innomedio.user.users.list'));
            $response->setMessage($this->get('translator')->trans('innomedio.user.users.submit.success.' . $action));
        } else {
            $response->setErrors($this->get('innomedio.backend_theme.error_parser')->getErrors($form, $user));
        }

        return new JsonResponse($response->getResponse());
    }

    /**
     * @Route("/remove/{id}", requirements={"id" = "\d+"}, name="innomedio.user.users.remove")
     * @param BackendUser $user
     * @return JsonResponse
     */
    public function ajaxRemove(BackendUser $user)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($user);
        $em->flush();

        return new JsonResponse(array(
            'success' => true
        ));
    }
}