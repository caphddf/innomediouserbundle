<?php
namespace Innomedio\UserBundle\Controller\Backend;

use Innomedio\BackendThemeBundle\Controller\BackendThemeController;
use Innomedio\BackendThemeBundle\Service\Ajax\AjaxResponse;
use Innomedio\UserBundle\Entity\BackendUserGroup;
use Innomedio\UserBundle\Form\BackendUserGroupType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Security("has_role('ROLE_BACKEND_GROUPS')")
 * @Route("/access/groups")
 */
class BackendUserGroupController extends BackendThemeController
{
    /**
     * @Route("/", name="innomedio.user.groups.list")
     * @return Response
     */
    public function list()
    {
        $this->header()->addBreadcrumb('innomedio.user.access', $this->generateUrl('innomedio.user.access'));
        $this->header()->addBreadcrumb('innomedio.user.user_groups');

        return $this->render('@InnomedioUser/backend/groups/list.html.twig', array(
            'groups' => $this->getDoctrine()->getRepository('InnomedioUserBundle:BackendUserGroup')->findAll()
        ));
    }

    /**
     * @Route(
     *     "/{action}/{id}",
     *     name="innomedio.user.groups.form",
     *     requirements={
     *         "action" = "add|edit",
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     * )
     *
     * @param $action
     * @param BackendUserGroup|null $group
     * @return Response
     */
    public function form($action, BackendUserGroup $group = null)
    {
        $this->header()->addBreadcrumb('innomedio.user.access', $this->generateUrl('innomedio.user.access'));
        $this->header()->addBreadcrumb('innomedio.user.user_groups', $this->generateUrl('innomedio.user.groups.list'));
        $this->header()->addBreadcrumb('innomedio.user.group.title.' . $action);

        $form = $this->createForm(BackendUserGroupType::class, $group);
        $form->add('submit', SubmitType::class, array(
            'label' => 'innomedio.backend_theme.global.' . $action
        ));

        return $this->render('@InnomedioUser/backend/groups/form.html.twig', array(
            'form' => $form->createView(),
            'activeNav' => 'access',
            'activeSubnav' => 'backend_groups'
        ));
    }

    /**
     * @Route(
     *     "/submit/{id}",
     *     name="innomedio.user.groups.submit"),
     *     requirements={
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     *
     * @param $id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function submit($id, Request $request)
    {
        $response = new AjaxResponse();

        if ($id > 0) {
            $group = $this->getDoctrine()->getRepository('InnomedioUserBundle:BackendUserGroup')->find($id);
            $action = 'edit';
        } else {
            $group = new BackendUserGroup();
            $action = 'add';
        }

        $form = $this->createForm(BackendUserGroupType::class, $group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();

            $response->setSuccess(true);
            $response->setRedirect($this->generateUrl('innomedio.user.groups.list'));
            $response->setMessage($this->get('translator')->trans('innomedio.user.groups.submit.success.' . $action));
        } else {
            $response->setErrors($this->get('innomedio.backend_theme.error_parser')->getErrors($form, $group));
        }

        return new JsonResponse($response->getResponse());
    }

    /**
     * @Route("/remove/{id}", requirements={"id" = "\d+"}, name="innomedio.user.groups.remove")
     * @param BackendUserGroup $group
     * @return JsonResponse
     */
    public function ajaxRemove(BackendUserGroup $group)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($group);
        $em->flush();

        return new JsonResponse(array(
            'success' => true
        ));
    }
}