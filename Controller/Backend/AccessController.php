<?php

namespace Innomedio\UserBundle\Controller\Backend;

use Innomedio\BackendThemeBundle\Controller\BackendThemeController;
use Innomedio\BackendThemeBundle\Service\Theme\ThemeManager;
use Symfony\Component\Routing\Annotation\Route;

class AccessController extends BackendThemeController
{
    /**
     * @Route("/access", name="innomedio.user.access")
     */
    public function index()
    {
        $this->header()->addBreadcrumb('innomedio.user.access');

        return $this->render('@InnomedioBackendTheme/item_page.html.twig', array(
            'type' => 'access'
        ));
    }
}
