<?php
namespace Innomedio\UserBundle\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="innomedio.user.login")
     * @return Response
     */
    public function index()
    {
        if ($this->getUser()) {
            return $this->redirectToRoute("innomedio.backend_theme.dashboard");
        }

        $error = $this->get('security.authentication_utils')->getLastAuthenticationError();
        $lastUsername = $this->get('security.authentication_utils')->getLastUsername();

        return $this->render('@InnomedioBackendTheme/security/login.html.twig', array(
            'error' => $error,
            'lastUsername' => $lastUsername,
            'loginAction' => $this->generateUrl('innomedio.user.login_submit'),
            'loginRedirect' => $this->generateUrl('innomedio.backend_theme.dashboard'),
            'loginError' => $error
        ));
    }

    /**
     * @Route("/login/submit", name="innomedio.user.login_submit")
     *
     * @param Request $request
     */
    public function loginAction(Request $request)
    {

    }

    /**
     * @Route("/logout", name="innomedio.user.logout")
     * @param Request $request
     */
    public function logoutAction(Request $request)
    {

    }

    /**
     * @Route("/", name="innomedio.user.anonymous_redirect");
     * @return RedirectResponse
     */
    public function cmsRedirect()
    {
        if ($this->getUser()) {
            return $this->redirectToRoute("innomedio.backend_theme.dashboard");
        }

        return $this->redirectToRoute('innomedio.user.login');
    }
}