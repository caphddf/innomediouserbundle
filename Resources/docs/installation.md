# Installation

**Make sure the InnomedioBackendThemeBundle is installed correctly.**

Add the bundle to your composer file: 

```
"require": {
    "innomedio/user-bundle": "^1.0"
},

"repositories": [
    {
        "type": "git",
        "url": "git@gitlab.com:innomedio/internal/symfony/InnomedioUserBundle.git"
    }
]
```

Add this to your environment file (don't forget to add it to your .env.dist file too)

```
###> innomedio/user-bundle###
BACKEND_SECURITY_PATTERN=^cms\.my-project\.com$
###< innomedio/user-bundle###
```

Add the routing file to your **config/routes/annotations.yaml** file:

```
innomedio_user:
    resource: '@InnomedioUserBundle/Resources/config/routes.yaml'
```

Update your security config file (you might need to change the backend links of you're using a different prefix):

```
security:
    encoders:
        Innomedio\UserBundle\Entity\BackendUser:
            algorithm: bcrypt

    providers:
        backend_user_provider:
            entity:
                class: Innomedio\UserBundle\Entity\BackendUser
                property: email

    firewalls:
        cms:
            host: "%env(BACKEND_SECURITY_PATTERN)%"
            pattern: ^/
            http_basic: ~
            provider: backend_user_provider
            anonymous: ~
            form_login:
                login_path: innomedio.user.login
                check_path: innomedio.user.login_submit
            logout:
                path: innomedio.user.logout
                target: innomedio.user.login

    access_control:
        - { path: ^/login, roles: IS_AUTHENTICATED_ANONYMOUSLY, host: "%env(BACKEND_SECURITY_PATTERN)%" }
        - { path: ^/, roles: ROLE_BACKEND, host: "%env(BACKEND_SECURITY_PATTERN)%" }
```

Generate all tables:

```
php bin/console doctrine:migrations:diff
php bin/console doctrine:migrations:migrate
```

Execute this command:

```
php bin/console innomedio:user:create_root
```

This will create an Innomedio group and an Innomedio user who is a member of that group. Only
users in that group can be root users and will automatically get the ROLE_ROOT role. This group (and the root users)
will never be visible to non-root users. 

The command will also add the ROLES needed in the InnomedioBackendThemeBundle and this bundle.

Once this bundle is installed, you should check the documentation of the other bundles to see which roles should
be added.