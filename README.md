# InnomedioUserBundle

Contains all entities and backend logic for managing users, user rights, and groups.

## Documentation

* [Installation](Resources/docs/installation.md)