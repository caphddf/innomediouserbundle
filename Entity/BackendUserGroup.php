<?php

namespace Innomedio\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="backend_user_group")
 * @ORM\Entity(repositoryClass="Innomedio\UserBundle\Repository\BackendUserGroupRepository")
 */
class BackendUserGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=70, unique=true)
     */
    private $name;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="BackendUser", mappedBy="group")
     */
    private $users;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $root = false;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="BackendUserRight", inversedBy="groups")
     * @ORM\JoinTable(name="backend_user_group_rights",
     *     joinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="right_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    private $rights;

    /**
     * BackendUserGroup constructor.
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->rights = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @return ArrayCollection|BackendUserRight[]
     */
    public function getRights()
    {
        return $this->rights;
    }

    /**
     * @param $rights ArrayCollection|BackendUserRight[]
     */
    public function setRights($rights)
    {
        $this->rights = $rights;
    }

    /**
     * @param BackendUserRight $right
     * @return bool
     */
    public function hasRight(BackendUserRight $right)
    {
        return $this->getRights()->contains($right);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isRoot(): bool
    {
        return $this->root;
    }

    /**
     * @param bool $root
     */
    public function setRoot(bool $root): void
    {
        $this->root = $root;
    }
}

