<?php
namespace Innomedio\UserBundle\Service\Backend;

use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarContainer;
use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarExtension;
use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarItem;
use Symfony\Component\Routing\Router;

class UserSidebarExtension extends SidebarExtension
{
    private $router;
    private $accessItem;

    /**
     * SidebarItems constructor.
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @return array|SidebarItem
     */
    public function getSidebars()
    {
        return array(
            $this->access(),
            $this->users(),
            $this->groups(),
            $this->rights()
        );
    }

    /**
     * @return SidebarItem
     */
    public function access()
    {
        $access = new SidebarItem();
        $access->setIcon('fa-users');
        $access->setName('innomedio.user.access');
        $access->setTag('access');
        $access->setLink($this->router->generate('innomedio.user.access'));
        $access->setOrder(98);

        $this->accessItem = $access;

        return $access;
    }

    /**
     * @return SidebarItem
     */
    public function users()
    {
        $usergroups = new SidebarItem();
        $usergroups->setIcon('fa-users');
        $usergroups->setName('innomedio.user.users');
        $usergroups->setLink($this->router->generate('innomedio.user.users.list'));
        $usergroups->setTag('backend_users');
        $usergroups->setParent($this->accessItem);
        $usergroups->setRole('ROLE_BACKEND_USERS');

        return $usergroups;
    }

    /**
     * @return SidebarItem
     */
    public function groups()
    {
        $usergroups = new SidebarItem();
        $usergroups->setIcon('fa-folder');
        $usergroups->setName('innomedio.user.user_groups');
        $usergroups->setLink($this->router->generate('innomedio.user.groups.list'));
        $usergroups->setTag('backend_groups');
        $usergroups->setParent($this->accessItem);
        $usergroups->setRole('ROLE_BACKEND_GROUPS');

        return $usergroups;
    }

    /**
     * @return SidebarItem
     */
    public function rights()
    {
        $userRights = new SidebarItem();
        $userRights->setIcon('fa-tag');
        $userRights->setName('innomedio.user.user_rights');
        $userRights->setLink($this->router->generate('innomedio.user.rights.list'));
        $userRights->setTag('backend_rights');
        $userRights->setParent($this->accessItem);
        $userRights->setRole('ROLE_ROOT');

        return $userRights;
    }
}