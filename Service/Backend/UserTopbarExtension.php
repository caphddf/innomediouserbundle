<?php
namespace Innomedio\UserBundle\Service\Backend;

use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarContainer;
use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarExtension;
use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarItem;
use Innomedio\BackendThemeBundle\Service\Topbar\TopbarExtension;
use Innomedio\BackendThemeBundle\Service\Topbar\TopbarItem;
use Symfony\Component\Routing\Router;

class UserTopbarExtension extends TopbarExtension
{
    /**
     * @return array|TopbarItem
     */
    public function getItems()
    {
        return array(
            $this->user()
        );
    }

    /**
     * @return TopbarItem
     */
    public function user()
    {
        $user = new TopbarItem();
        $user->setPosition('right');
        $user->setTag('user');
        $user->setTemplate('@InnomedioUser/backend/topbar/user.html.twig');

        return $user;
    }
}