# TODO 1.0.4

* Added the settings role to the setup command

# 1.0.3

* Changed route file for BackendThemeBundle 1.1 change (subdomain) and the documentation for implementation.
* Removed the unnecessary setup command

# 1.0.2

* Added a choice_translation_domain in the BackendUserGroupType

# 1.0.1

* Gave the user sidebar a high sort order

# 1.0

* Start