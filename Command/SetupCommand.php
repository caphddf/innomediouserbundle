<?php
namespace Innomedio\UserBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Innomedio\UserBundle\Entity\BackendUser;
use Innomedio\UserBundle\Entity\BackendUserGroup;
use Innomedio\UserBundle\Entity\BackendUserRight;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SetupCommand extends Command
{
    private $fs;
    private $projectDir;
    private $em;
    private $encoder;

    /**
     * SetupCommand constructor.
     */
    public function __construct(Filesystem $fs, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder, $projectDir)
    {
        $this->fs = $fs;
        $this->projectDir = $projectDir;
        $this->em = $em;
        $this->encoder = $encoder;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('innomedio:user:create_root')
            ->setDescription('Set up the InnomedioUserBundle')
            ->setHelp('Prepares the project for this bundle');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>1. Create Innomedio user</info>');

        $bytes = random_bytes(5);
        $password = bin2hex($bytes);

        $user = new BackendUser();
        $encoded = $this->encoder->encodePassword($user, $password);

        $user->setUsername('info@innomedio.be');
        $user->setEmail('info@innomedio.be');
        $user->setFirstName('Innomedio');
        $user->setPassword($encoded);

        $this->em->persist($user);
        $this->em->flush();

        $output->writeln('Innomedio user created with this password: ' . $password);

        $output->writeln('<info>2. Create Innomedio group</info>');
        $group = new BackendUserGroup();
        $group->setName('Innomedio');
        $group->setRoot(true);

        $output->writeln('<info>3. Insert roles</info>');

        $roles = array('ROLE_BACKEND_USERS', 'ROLE_BACKEND_GROUPS', 'ROLE_LANGUAGES', 'ROLE_SETTINGS');
        foreach ($roles as $role) {
            $right = new BackendUserRight();
            $right->setName($role);
            $this->em->persist($right);
            $this->em->flush();

            $output->writeln($role . ' created');

            $group->getRights()->add($right);
        }

        $this->em->persist($group);
        $this->em->flush();

        $user->setGroup($group);
        $this->em->persist($user);
        $this->em->flush();

        $output->writeln('<info>Done!</info>');
    }
}