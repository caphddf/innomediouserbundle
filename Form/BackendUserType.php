<?php

namespace Innomedio\UserBundle\Form;

use Doctrine\ORM\EntityRepository;
use Innomedio\UserBundle\Entity\BackendUser;
use Innomedio\UserBundle\Entity\BackendUserGroup;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BackendUserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, array('label' => 'innomedio.backend_theme.global.first_name'))
            ->add('lastName', TextType::class, array('label' => 'innomedio.backend_theme.global.last_name'))
            ->add('email', EmailType::class, array('label' => 'innomedio.backend_theme.global.email'))
            ->add('password', PasswordType::class, array('label' => 'innomedio.backend_theme.global.password', 'required' => is_null($builder->getData()->getId())))
            ->add('group', EntityType::class, array(
                'class' => BackendUserGroup::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('g')
                        ->where('g.root = 0');
                },
                'multiple' => false,
                'expanded' => false,
                'choice_label' => 'name',
                'label' => 'innomedio.user.users.group',
                'required' => true,
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => BackendUser::class,
            'allow_extra_fields' => true
        ));
    }
}