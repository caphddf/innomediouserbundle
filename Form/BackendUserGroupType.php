<?php

namespace Innomedio\UserBundle\Form;

use Innomedio\UserBundle\Entity\BackendUserGroup;
use Innomedio\UserBundle\Entity\BackendUserRight;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BackendUserGroupType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('label' => 'innomedio.backend_theme.global.name'))
            ->add('rights', EntityType::class, array(
                'class' => BackendUserRight::class,
                'multiple' => true,
                'expanded' => true,
                'choice_translation_domain' => 'messages',
                'choice_label'  => function ($right) {
                    return 'innomedio.' . strtolower($right->getName());
                },
                'label' => 'innomedio.user.group.rights'
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => BackendUserGroup::class,
            'allow_extra_fields' => true
        ));
    }
}